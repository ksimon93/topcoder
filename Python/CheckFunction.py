#Given an input string of numbers returns number of dashes each number is composed of as if it were being displayed on digital clock.
#Author: Kevin Simon
#Date: 1/25/14

import sys
def newFunction(input):
	numToDash = {'0':6, '1':2, '2':5, '3':5, '4':4, '5':5, '6':6, '7':3, '8':7, '9':6}
	sum = 0
	for ch in input:
		sum += numToDash[ch]

	return sum

if __name__ == '__main__':
    print(newFunction(sys.argv[1]))
